package requirements;

import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * И привычная нам работа с WD с PageObject.
 */
public class BookPage extends PageObject {
    @FindBy(linkText = "Все книги")
    org.openqa.selenium.WebElement allbooksButton;
    @FindBy(linkText = "Поиск")
    WebElement searchButton;
    @FindBy(name = "query")
    WebElement searchField;
    @FindBy(css = "button")
    WebElement searchBegin;

    public BookPage(WebDriver driver) {
        super(driver);
    }

    public void getMainPage(String url) {
        getDriver().get(url);

    }

    public void allBooks() {
        allbooksButton.click();
    }

    public void search(String searchWord) {
        searchButton.click();
        searchField.sendKeys(searchWord);
        searchBegin.click();

    }

    public void catalog() {
    }
}