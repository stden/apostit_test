package requirements;

import net.thucydides.core.annotations.*;
import net.thucydides.core.pages.Pages;
import net.thucydides.junit.runners.ThucydidesRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

/**
 * Created by den on 10.06.13.
 */
@RunWith(ThucydidesRunner.class)
@Story(Application.TestPageBook.class) // Указываем Story для тестов относительно нее будет сформирован отчет
public class TestPageBook {
    @Managed
    public WebDriver driver;
    @ManagedPages(defaultUrl = "http://m.megafonpro.ru/")
    public Pages pages;
    @Steps
    public StepsinBook book;

    @Test
    public void testBook() throws Exception {
        book.getMain("http://m.megafonpro.ru/"); // Вход на главную страницу тестируемого ресурса
        book.AllBooks();                   // Переход во вкладку Все книги
        book.search();                     // Поиск книги с поисковым выражением "Книга
        book.catalog();                    // Проверка каталога книг по жанрам
    }

    @Pending
    @Test // данная аннотация значит,что тест еще не имплементирован
    public void testBuyBook() {
    }

}